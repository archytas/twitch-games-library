<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FindGamesRequest;
use App\Services\GamesService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class GamesController extends Controller
{

    /**
     * Find games for given search query
     *
     * @param FindGamesRequest $request
     * @param GamesService $gamesService
     * @return JsonResponse
     */
    public function find(FindGamesRequest $request, GamesService $gamesService): JsonResponse
    {
        $query = $request->get('query', '');

        // event log would be nice for this purpose (e.g. spatie activitylog to put it in db)
        Log::info('User searches game', compact('query'));

        $games = $gamesService->search($query);

        return response()->json($games);
    }
}
