<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FindGamesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // for simplicity (PoC) we assume one does not have to be logged in.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query' => ['required']
        ];
    }
}
