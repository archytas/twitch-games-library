<?php


namespace App\Services;


use App\Exceptions\InvalidApiResponse;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class GameSpotService implements GameFinderInterface
{

    protected $baseUrl = '';
    protected $client;
    protected $key = '';

    /**
     * RawgGamesService constructor.
     * @param Client|null $client
     */
    public function __construct(Client $client = null)
    {
        $this->key = config('services.gamespot.key');
        $this->baseUrl = 'http://www.gamespot.com/api/';
        $this->client = $client ?? new Client([
                'base_uri' => $this->baseUrl,
                'headers' => [
                    'Content-Type' => 'application/json; charset=utf-8',
                    'Accept' => 'application/json; charset=utf-8'
                ]
            ]);
    }

    /**
     * @param string $query
     * @return \Illuminate\Support\Collection
     * @throws InvalidApiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getGames(string $query): Collection
    {
        // remove characters with functional meaning (not sure if it is possible to escape them)
        $query = str_replace([',', ':'], '', $query);

        try {
            $response = $this->client->request('GET', 'games', [
                'query' => [
                    'api_key' => $this->key,
                    'format' => 'json',
                    'filter' => "name:" . $query
                ]
            ]);
        } catch (\Exception $e) {
            Log::error('Failed querying GameSpot API');
            return collect([]);
        }

        if ($response->getStatusCode() !== 200) {
            throw new InvalidApiResponse('Failed to fetch games: ' . $response->getReasonPhrase() ?? '');
        }

        $data = json_decode($response->getBody()->getContents(), false);

        return $this->responseToGamesCollection($data);
    }

    /**
     * Sanitize/map games data
     *
     * Might move it to a separate mapper.
     *
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    protected function responseToGamesCollection($data)
    {
        if ($data->error !== "OK" || $data->number_of_total_results < 1) {
            return collect([]);
        }
        $games = collect([]);
        foreach ($data->results as $result) {

            // some release dates before 2000 should be decremented with 100 years (bug)
            $date = $result->release_date;
            if (isset($date)) {
                $cd = new Carbon($date);
                $date = $cd->toDateString();
            }

            $games->push([
                'name' => $result->name,
                'image' => optional($result->image)->square_small,
                'release_date' => $date,
                'source' => 'GameSpot'
            ]);
        }
        return $games;
    }
}
