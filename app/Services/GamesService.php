<?php


namespace App\Services;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class GamesService
{

    /**
     * List of services (api wrappers) that support the GameFinderInterface.
     * @var string[]
     */
    protected $services = [
        GameSpotService::class,
        RawgGamesService::class,
        IgdbGamesService::class
    ];

    /**
     * @param string $query
     * @return \Illuminate\Support\Collection
     */
    public function search(string $query): Collection
    {
        $games = collect([]);
        try {
            foreach ($this->services as $service) {
                $service = new $service();
                $newGames = $service->getGames($query);
                $games = $games->merge($newGames);
            }
            // remove duplicate titles
            $uniqueGames = $games->unique('name')->values();
        } catch (\Exception $e) {
            Log::error('Failed to search for game: ' . $e->getMessage());
            $uniqueGames = collect([]);
        }
        return $uniqueGames;
    }

}
