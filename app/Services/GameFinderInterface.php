<?php


namespace App\Services;


use Illuminate\Support\Collection;

interface GameFinderInterface
{

    public function getGames(string $query): Collection;
}
