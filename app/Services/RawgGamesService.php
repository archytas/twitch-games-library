<?php


namespace App\Services;

use App\Exceptions\InvalidApiResponse;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class RawgGamesService implements GameFinderInterface
{
    protected $baseUrl = '';
    protected $client;
    protected $key = '';

    /**
     * RawgGamesService constructor.
     * @param Client|null $client
     */
    public function __construct(Client $client = null)
    {
        $this->key = config('services.cloud-vision.key');
        $this->baseUrl = 'https://api.rawg.io/api/';
        $this->client = $client ?? new Client([
                'base_uri' => $this->baseUrl,
                'headers' => [
                    'Content-Type' => 'application/json; charset=utf-8',
                    'Accept' => 'application/json; charset=utf-8'
                ]
            ]);
    }

    /**
     * Find games matching given search query
     *
     * @param $query
     * @return \Illuminate\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException|InvalidApiResponse
     */
    public function getGames($query): Collection
    {
        try {
            $response = $this->client->request('GET', 'games', [
                'query' => [
                    'key' => $this->key,
                    'search' => $query
                ]
            ]);
        } catch (\Exception $e) {

            return collect([]);
        }

        if ($response->getStatusCode() !== 200) {
            throw new InvalidApiResponse('Failed to fetch games: ' . $response->getReasonPhrase() ?? '');
        }

        $data = json_decode($response->getBody()->getContents(), false);

        return $this->responseToGamesCollection($data);
    }

    /**
     * Sanitize/map games data
     *
     * Might move it to a separate mapper.
     *
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    protected function responseToGamesCollection($data): Collection
    {
        if (!$data->count) {
            return collect([]);
        }
        $games = collect([]);
        foreach ($data->results as $result) {
            $games->push([
                'name' => $result->name,
                'image' => $result->background_image,
                'release_date' => $result->released,
                'source' => 'RAWG'
            ]);
        }
        return $games;
    }
}
