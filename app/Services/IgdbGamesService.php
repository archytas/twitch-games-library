<?php


namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use MarcReichel\IGDBLaravel\Models\Cover;
use MarcReichel\IGDBLaravel\Models\Game;
use MarcReichel\IGDBLaravel\Models\Screenshot;

class IgdbGamesService implements GameFinderInterface
{


    /**
     * @param $query
     * @return \Illuminate\Support\Collection
     */
    public function getGames(string $query): Collection
    {
        $rawGameCollection = collect([]);
        try {
            $rawGameCollection = Game::select(['name', 'first_release_date', 'screenshots', 'cover'])->search($query)->get();
        } catch (\Exception $e) {
            Log::error('Failed to query IGDB API succesfully: ' . $e->getMessage());
            return $rawGameCollection;
        }

        return $this->responseToGamesCollection($rawGameCollection);
    }

    /**
     * Sanitize/map games data
     *
     * Might move it to a separate mapper.
     *
     * @param $data
     * @return \Illuminate\Support\Collection
     */
    protected function responseToGamesCollection($data): Collection
    {
        $games = collect([]);
        foreach ($data as $game) {
            $image = null;
            if (isset($game->screenshots) && count($game->screenshots)) {
                $imgData = Screenshot::find($game->screenshots[0]);
                $image = $imgData->url;
            } elseif (isset($game->cover) && is_numeric($game->cover)) {
                $cover = Cover::find($game->cover);
                $image = $cover->url ?? null;
            }
            if (isset($image)) {
                $image = str_replace('t_thumb', 't_cover_big', $image);
            }
            $games->push([
                'name' => $game->name,
                'release_date' => isset($game->first_release_date) ? $game->first_release_date->toDateString() : null,
                'image' => $image ?? null,
                'source' => 'IGDB'
            ]);
        }
        return $games;
    }
}
