
let gameFinderForm = document.getElementById('gameFinderForm');

let spinner = '<div class="p-4 text-center"><div class="spinner-border" role="status">\n' +
    '  <span class="visually-hidden">Loading...</span>\n' +
    '</div></div>';

/**
 * Find (and display) results when searching for a game
 */
gameFinderForm.addEventListener("submit", function (event) {
    event.preventDefault();

    let query = document.getElementsByName('query')[0].value;
    let gameResultsEl = document.getElementById('gameResults');
    gameResultsEl.innerHTML = spinner;

    axios.post('/api/games', {
        query: query
    })
        .then(function (response) {

            let html = '';
            if (response.data.length === 0) {
                html = '<p>No results</p>';
            }
            // build result cards
            for (const game of response.data) {
                html += getGameCard(game);
            }
            // show result cards
            gameResultsEl.innerHTML = html;

        })
        .catch(function (error) {
            // handle error
            console.log(error);
            gameResultsEl.innerHTML = '<div class="alert alert-warning">Something went wrong. Did you provide a search query?</div>';
        })
});

/**
 * Format game as html card
 *
 * @param game
 */
function getGameCard(game)
{
    // a Vue template can be used when the site uses Vue.js (for PoC I use plain html here).
    html = '<div class="col-xl-3 col-lg-4 col-md-6"><div class="card mb-3">';
    if (game.image !== null) {
        html += '<img src="' + game.image + '" class="card-img-top" alt="game">';
    }
    html += '<div class="card-body">';
    if (game.source !== null) {
        html += '<p class="source text-muted">Source: ' + game.source + '</p>';
    }
    if (game.name !== null) {
        html += '<h5 class="card-title">' + game.name + '</h5>';
    }
    html += '<p class="card-text">';
    if (game.release_date !== null) {
        html += game.release_date + ''; // tbd: localise
    }
    html += '</p>';
    if (game.name !== null) {
        html += '<a onclick="alert(\'' + game.name + ' has been added to your collection\')" href="#" class="btn btn-primary">Add to collection</a>';
    }
    html += '</div>';
    html += '</div></div>';

    return html;
}
