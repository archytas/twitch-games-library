<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class SearchGamesTest extends DuskTestCase
{
    /**
     * Test Library page
     *
     * @return void
     */
    public function testGamesLibraryIsPresent()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/games')
                    ->assertSee('Games Library');
        });
    }
}
