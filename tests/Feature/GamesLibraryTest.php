<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GamesLibraryTest extends TestCase
{
    /**
     * Check page is present
     *
     * @return void
     */
    public function test_games_view_will_be_served()
    {
        $response = $this->get('/games');
        $response->assertStatus(200);
    }

}
